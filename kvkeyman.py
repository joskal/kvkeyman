#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
/***************************************************************************
KvKeyMan - a super tiny password manager. Data stored in a kdbx file.

 This is just a small gui for [2], bsed on [1].

 [1](https://github.com/PySimpleGUI/PySimpleGUI)
 [2](https://github.com/libkeepass/pykeepass)
                             -------------------
        begin                : 2022-02-23
        copyright            : (C) 2022 by joskal
        email                : groundwatergis [at] gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
'''
USAGE:
python kvkeyman.py -db="C:/data/test.kdbx"
'''
import argparse, os, io, base64
import pandas as pd
from PIL import Image

# -------- fix for launching with pythonw.exe
import sys, os
if sys.executable.endswith("pythonw.exe"):
  sys.stdout = open(os.devnull, "w");
  sys.stderr = open(os.path.join(os.getenv("TEMP"), "stderr-"+os.path.basename(sys.argv[0])), "w")

from pykeepass import PyKeePass, create_database
from pykeepass.exceptions import CredentialsError
import PySimpleGUI as sg

"""
sg_themes = ['Black', 'BlueMono', 'BluePurple', 'BrightColors', 'BrownBlue', 'Dark', 'Dark2', 'DarkAmber', 'DarkBlack', 'DarkBlack1'
, 'DarkBlue', 'DarkBlue1', 'DarkBlue10', 'DarkBlue11', 'DarkBlue12', 'DarkBlue13', 'DarkBlue14', 'DarkBlue15', 
'DarkBlue16', 'DarkBlue17', 'DarkBlue2', 'DarkBlue3', 'DarkBlue4', 'DarkBlue5', 'DarkBlue6', 'DarkBlue7', 'DarkBlue8', 
'DarkBlue9', 'DarkBrown', 'DarkBrown1', 'DarkBrown2', 'DarkBrown3', 'DarkBrown4', 'DarkBrown5', 'DarkBrown6', 
'DarkGreen', 'DarkGreen1', 'DarkGreen2', 'DarkGreen3', 'DarkGreen4', 'DarkGreen5', 'DarkGreen6', 'DarkGrey', 
'DarkGrey1', 'DarkGrey2', 'DarkGrey3', 'DarkGrey4', 'DarkGrey5', 'DarkGrey6', 'DarkGrey7', 'DarkPurple', 'DarkPurple1', 
'DarkPurple2', 'DarkPurple3', 'DarkPurple4', 'DarkPurple5', 'DarkPurple6', 'DarkRed', 'DarkRed1', 'DarkRed2', 
'DarkTanBlue', 'DarkTeal', 'DarkTeal1', 'DarkTeal10', 'DarkTeal11', 'DarkTeal12', 'DarkTeal2', 'DarkTeal3', 'DarkTeal4',
'DarkTeal5', 'DarkTeal6', 'DarkTeal7', 'DarkTeal8', 'DarkTeal9', 'Default', 'Default1', 'DefaultNoMoreNagging', 'Green', 
'GreenMono', 'GreenTan', 'HotDogStand', 'Kayak', 'LightBlue', 'LightBlue1', 'LightBlue2', 'LightBlue3', 'LightBlue4', 
'LightBlue5', 'LightBlue6', 'LightBlue7', 'LightBrown', 'LightBrown1', 'LightBrown10', 'LightBrown11', 'LightBrown12', 
'LightBrown13', 'LightBrown2', 'LightBrown3', 'LightBrown4', 'LightBrown5', 'LightBrown6', 'LightBrown7', 'LightBrown8',
'LightBrown9', 'LightGray1', 'LightGreen', 'LightGreen1', 'LightGreen10', 'LightGreen2', 'LightGreen3', 'LightGreen4', 
'LightGreen5', 'LightGreen6', 'LightGreen7', 'LightGreen8', 'LightGreen9', 'LightGrey', 'LightGrey1', 'LightGrey2', 
'LightGrey3', 'LightGrey4', 'LightGrey5', 'LightGrey6', 'LightPurple', 'LightTeal', 'LightYellow', 'Material1', 
'Material2', 'NeutralBlue', 'Purple', 'Reddit', 'Reds', 'SandyBeach', 'SystemDefault', 'SystemDefault1', 
'SystemDefaultForReal', 'Tan', 'TanBlue', 'TealMono', 'Topanga']
"""
sg.theme('SystemDefault')#'Light green 6', 'DarkAmber'


# ----------- embedded images ---------------
kvkeyman_icon = b'iVBORw0KGgoAAAANSUhEUgAAADkAAAA5CAIAAAADehTSAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAASkSURBVGhD7ZjPK2VhGMffsiDj3lEkTRH5kRhRSsjCjCEyC6X8KEVp/AopG1JEyUoWFoqFP4GSlAV2lmxkhRLJZCIW5l5y5ut9nnO87o/jXO4xYzqfzuLe9/nxfu9z3vd5z7lCez84Wu3B0WoPjlZ7cLTagwWtt16tIVtr+vx4NeZod7dsBXUZT6y4VGv4sKLVo31xaV/djxe+4gcYlEU/seJCiA04Wh2t/5lWr0ercGmVHx+vCvfDoMG3D0+suLy/2RRWLGj9Z3C02kNoWpeXlxsaGjIyMj7p5OXltbS0LC0tsYfk7OzspwK+skGHDTqXl5c0fnBwMDg42NnZ2dXVRSMqVrVWVVXFxsaK4LhcrtnZ2ZubGzi3t7f/UOjo6KAkBmzQmZmZweDAwAA+IxZALnmqPK91aGgIOljRc8TExCwsLEAcTUn4F4kNOtA6NTVlCAUv0VpTU8MqQqGgoIDnlDyr1Z+QtZaUlPDkOhEREWlpaeXl5d91iouL8/Pzo6Oj2UMnMzOTp30Drb29vTytTkJCQn19PRLJNfYDCgClbmtrg2gfxUZ17dV6fHyMEvKcspyFhYVIAYkjIyPspHN/fz8+Pg5Ta2trXFwcx8iouro6RIWkdWJi4urqiv2eElgr7iBPKMG9Rhaoub6+Zo9A+MvFZ5TculY2ByGA1u3tbZ5Kkp2drSZCV5qenoamsrKy2tra0dHR09NTMt3d3cENJv/l+2IwCyUHAbSilbKj7JooDBRsbm7CtLW1lZWVxTYd9F30KYrFekB1sXbZ9mqe0ep2u9lRCGx5WdOHoqJ+JsfB6uoqhaOl4+eFq7RmWvf29thLbg4q6traGkyNjY1sCERiYuLFxQXcNjY2EJKbm8uG12GmdX19nb2EwHGPWXFPcaBjmfKoBIsSmiYnJ6OionhIiMXFRUrycCMUrOwtNkiQmTOaa52fn2cvIVJSUigRhKrxGGdvTYNoHhWiv7+fBinKwC6tOJrZSwgcUZTI4/GgZjwqRFFREXtrGh6LeFQIo4HjVlAgYZfWlZUV9lLWAJoz1iKPSmjjoy1gmfKQPghoegO7tKrNFXuZEu3v78OEc58NgcDCPTw8hNvJyQlCcM6xIThUC0LVp2KmFUszMjKSHfUTq6enByafM8IH7DPKMDc3Z7FnvVYrwKM+OyrpvN6Hl2w8Tasb3wA7jGKB9bMgDFrVbQRKS0uRy9g3aMA4WtEKYMLRgFzGKQD6+vqam5stHgRh0Ari4+PZV54ITU1NSNfd3X17a/YHIB7BcPeTkpI4Uq54SFf3FvYf24JrVfWpBNaKdz2Ok6B+qCUlxXJkJ4WdnR28t/gIBZWVlQixVyvwfylAH6AjFysSyrDh0Pyhg7opdiEextlVYrwaBNMajJC1guTkZI7WQYGxfHFbSQRAvVE8VIg9dKAbG4583kIr+ld6ejonCAVE0R0g3kIrUV1dzTmsgYVh8s5tr1awu7ubk5PDmYKDm46OBv9Q/x8AbDDFklbi6OhoeHg4NTVVbZ94o8Kj6tjYmPEmA87Pz38p4CsbdNigwAZTQtD613G02oOj1R4crfbgaLWH96NV0/4ALmb8i0RCqRQAAAAASUVORK5CYII='


class KeyMan():
    def __init__(self, dbpath='', pw = None):
        self.dbpath = dbpath
        self.group = None
        self.entry = None
        self.user = None
        self.pw = pw
        self.keydb = None
        
    def load_keydb(self):
        try:
            self.keydb = PyKeePass(self.dbpath, password=self.pw)
            return True
        except CredentialsError:
            print('password not accepted')
            sg.popup_error('Error', 'Password not accepted')
            return False
        except:
            print('unkown error')
            sg.popup_error('Error', 'unknown error')
            return False
    
    def load_entries_into_table(self):
        # group, title, username, pw, url, notes
        data = []
        for entry in self.keydb.entries:
            tags_as_string = self.tags2string(entry.tags)
            #if entry.tags is None:
            #    tags_as_string = ''
            #else:
            #    tags_as_string = ','.join(entry.tags)
            #data.append([entry.group.name,entry.title,entry.username,entry.password,entry.url,entry.notes,tags_as_string])
            data.append([entry.group.name,entry.title,entry.username,entry.url,entry.notes,tags_as_string])
        self.entries_table = data

    def tags2string(self,tags_entry=None):
        if tags_entry is None:
            return ''
        else:
            return ','.join(tags_entry)        


class MainApp():
    def __init__(self,km):
        self.km = km
        self.group = None
        self.entry = None
        self.create_main_window()
        self.run()

    def add_or_duplicate_entry(self):
        if self.entry and self.group:    
            group_name = self.group.name
            window = self.edit_window('Duplicate Entry')
        else:
            group_name = ''
            window = self.edit_window('Add Entry')

        while True:  
            event, values = window.read()
            if event == "-CANCEL-" or event == sg.WIN_CLOSED:
                window.close()
                return     
            if event == "-SUBMIT-":
                if not(values['-GROUPNAME-']==group_name):# group changed?
                    new_group = self.km.keydb.find_groups(
                                                name=values['-GROUPNAME-'], 
                                                first=True)
                    if new_group is None: # new group not in db?
                        self.group = self.km.keydb.add_group(
                                            self.km.keydb.root_group,
                                            values['-GROUPNAME-'])
                    else: 
                        self.group = new_group
            
                self.entry = self.km.keydb.add_entry(self.group,
                                    values['-TITLE-'], 
                                    values['-USERNAME-'], 
                                    values["-PW-"], 
                                    url=values["-URL-"], 
                                    notes=values["-NOTES-"], 
                                    tags=list(set(values["-TAGS-"].split(","))))#set to get unique

                self.km.keydb.save()
                window.close()
                return            

    def create_main_window(self):
        sg.set_options(element_padding=(0, 0))

        # ------ Menu Definition ------ #
        menu_def = [['&File', ['&New database', '&Open database', '&Exit' ]],
                    ['&Edit', ['&Add Entry','&Edit Entry','&Duplicate Entry','&Delete Entry'],],
                    ['&View', ['&View Entry'],],
                    #['&Toolbar', ['---', 'Command &1', 'Command &2', '---', 'Command &3', 'Command &4']],
                    ['&Help', '&About...'],]

        right_click_menu = ['Unused', ['Copy password, Ctrl-c',
                                        'Copy username, Ctrl-b',
                                        'Copy url, Ctrl-u',
                                        'Edit Entry, Ctrl-e',
                                        'View Entry, Ctrl-w'
                                        ]]

        # ------ GUI Defintion ------ #
        gui_width = 75
        tab_headings = ['group', 'title', 'username']
        layout = [
                [sg.MenubarCustom(menu_def, tearoff=False)],
                [sg.Text('Status:'), sg.Text(size=(gui_width-(len('Status:')+1),1), key='-KDBX_FILE-',expand_x=True)],
                [sg.Text('Filter:', size =(len('Filter:')+1, 1)), 
                    sg.InputText(key='-FILTER-',
                                size=(gui_width-(len('Filter:')+1),1),
                                expand_x=True,
                                focus=True,
                                enable_events=True)],
                [sg.Table(values=[['']], 
                        #headings=['group', 'title', 'username', 'pw', 'url', 'notes','tags'], 
                        headings=tab_headings,
                        max_col_width=gui_width//len(tab_headings),
                        select_mode=sg.TABLE_SELECT_MODE_BROWSE, #ej multiselect
                        auto_size_columns=True,
                        display_row_numbers=False,
                        justification='left',
                        num_rows=10,
                        #alternating_row_color='lightyellow',
                        key='-TABLE-',
                        selected_row_colors='red on yellow',
                        enable_events=True,
                        expand_x=True,
                        expand_y=True,
                        enable_click_events=True,
                        right_click_selects=True,
                        bind_return_key=True
                        )],
                [sg.Text('URL:'), sg.Text(size=(60,1), key='-URL-')],
                [sg.HorizontalSeparator()],
                [sg.Text('Notes:'), sg.Text(size=(60,5), key='-NOTES-',
                                            tooltip='Edit entry to read more lines')],
                [sg.HorizontalSeparator()],
                [sg.Text('Tags:'), sg.Text(size=(60,1), key='-TAGS-')],
                [sg.HorizontalSeparator()],
                [sg.Text('Debug:'), sg.Text(size=(60,1))],
                [sg.Multiline(key='-OUT-',
                              enter_submits=False,
                              disabled = True,
                              size=(75,2), 
                              font=('Helvetica 10'),
                              autoscroll=True,
                              horizontal_scroll=True, 
                              write_only=True,
                              auto_refresh=True, # may cause conflixt with selected rows?
                              reroute_cprint=True, 
                              echo_stdout_stderr=True,
                              expand_x=True)]
                ]
        # ------ GUI Defintion ------ #          
        size = (16,16)
        image = convert_to_bytes(kvkeyman_icon, size, fill=False)
        self.window = sg.Window("KvKeyMan - a tiny password manager",
                        layout,
                        #use_custom_titlebar = True, # förhindrar resizable!
                        default_element_size=(12, 1),
                        grab_anywhere=True,
                        right_click_menu=right_click_menu,
                        default_button_element_size=(12, 1),
                        resizable=True,
                        finalize=True,
                        icon=image,
                        use_default_focus=True)
        self.window['-TABLE-'].block_focus(block = False)
        self.window['-TABLE-'].bind("<Return>", "_Enter")
        self.window.bind("<Control-KeyPress-f>", "CTRL-f")
        self.window.bind("<Control-KeyPress-t>", "CTRL-t")
        self.window.bind("<Control-KeyPress-c>", "CTRL-c")
        self.window.bind("<Control-KeyPress-b>", "CTRL-b")
        self.window.bind("<Control-KeyPress-u>", "CTRL-u")
        self.window.bind("<Control-KeyPress-e>", "CTRL-e")
        self.window.bind("<Control-KeyPress-w>", "CTRL-w")

    def delete_or_view_window(self,title,buttons=2):
        layout = [
            [sg.Text('group:',size=(10,1)),sg.Text(key='-GROUPNAME-',size=(25,1),expand_x=True)],
            [sg.Text('title:',size=(10,1)),sg.Text(key="-TITLE-",size=(25,1),expand_x=True)],
            [sg.Text('username:',size=(10,1)),sg.Text(key="-USERNAME-",size=(25,1),expand_x=True)],
            [sg.Text('password:',size=(10,1)),sg.Text(key="-PW-",size=(25,1),expand_x=True)],
            [sg.Text('url:',size=(10,1)),sg.Text(key="-URL-",size=(25,1),expand_x=True)],
            [sg.Text('notes:',size=(10,1)),sg.Text(key="-NOTES-",size=(25,1),expand_x=True)],
            [sg.Text('tags:',size=(10,1)),sg.Text(key="-TAGS-",size=(25,1),expand_x=True)],
            [sg.Cancel(key="-CANCEL-"), sg.OK(key="-OK-")] if buttons ==2 else [sg.OK(key="-OK-")],
            ]

        window = sg.Window(title, layout,finalize=True)

        window['-GROUPNAME-'].update(self.group.name)
        window['-TITLE-'].update(self.entry.title)
        window['-USERNAME-'].update(self.entry.username)
        window['-PW-'].update(self.entry.password)        
        window['-URL-'].update(self.entry.url)
        window['-NOTES-'].update(self.entry.notes)
        window['-TAGS-'].update(self.km.tags2string(self.entry.tags))
        
        return window

    def edit_window(self,title):
        layout = [[sg.Text('group')],[sg.InputText(key='-GROUPNAME-')],
                [sg.Text('title')],[sg.InputText(key="-TITLE-")],
                [sg.Text('username')],[sg.InputText(key="-USERNAME-")],
                [sg.Text('password')],[sg.InputText(key="-PW-")],
                [sg.Text('url')],[sg.InputText(key="-URL-")],
                [sg.Text('notes')],[sg.Multiline(key="-NOTES-",size=(40,5))],
                [sg.Text('tags')],[sg.InputText(key="-TAGS-")],
                [sg.Cancel(key="-CANCEL-"), sg.Submit(key="-SUBMIT-")]
                ]
        window = sg.Window(title, layout,finalize=True)

        if self.entry is None:
            return window

        window['-GROUPNAME-'].update(self.group.name)
        window['-TITLE-'].update(self.entry.title)
        window['-USERNAME-'].update(self.entry.username)
        window['-PW-'].update(self.entry.password)        
        window['-URL-'].update(self.entry.url)
        window['-NOTES-'].update(self.entry.notes)
        window['-TAGS-'].update(self.km.tags2string(self.entry.tags))

        return window

    def delete_entry(self):
        if self.entry is None or self.group is None:
            sg.popup_error('Error', 'No entry selected!')    
            return

        window = self.delete_or_view_window('Delete Entry',buttons=2)
        
        while True:  
            event, values = window.read()
            if event == "-CANCEL-" or event == sg.WIN_CLOSED:
                window.close()
                return     
            if event == "-OK-":
                yesno = sg.popup_yes_no(f'Delete {self.entry.title}?') 
                if yesno=='Yes':
                    self.km.keydb.delete_entry(self.entry)
                    self.km.keydb.save()
                window.close()
                return        

    def edit_entry(self):    
        if self.entry is None: 
            sg.popup_error('Error', 'No entry selected!')
            print('must select row to edit an entry')
            self.window['-OUT-'].print(f'None selected')
            return

        window = self.edit_window('Edit Entry')

        while True:  
            event, values = window.read()
            if event == "-CANCEL-" or event == sg.WIN_CLOSED:
                window.close()
                return     
            if event == "-SUBMIT-":
                if not(values['-GROUPNAME-']==self.group.name):# group changed?
                    new_group = self.km.keydb.find_groups(
                                                name=values['-GROUPNAME-'], 
                                                first=True)
                    if new_group is None: # new group not in db?
                        self.group = self.km.keydb.add_group(
                                            self.km.keydb.root_group,
                                            values['-GROUPNAME-'])
                    else: 
                        self.group = new_group
                    # new group so create new entry in this group
                    self.entry = self.km.keydb.add_entry(self.group,
                                    values['-TITLE-'], 
                                    values['-USERNAME-'], 
                                    values["-PW-"], 
                                    url=values["-URL-"], 
                                    notes=values["-NOTES-"], 
                                    tags=values["-TAGS-"].split(","))
                    sg.popup(f'New entry created in group {self.group.name}.'\
                                'Old entry remains in old group.')
                else: # same group
                    tags_as_list = list(set(values["-TAGS-"].split(",")))#set to get unique
                    self.entry.title = values['-TITLE-']
                    self.entry.username = values['-USERNAME-']
                    self.entry.password = values["-PW-"]
                    self.entry.url = values["-URL-"] 
                    self.entry.notes=values["-NOTES-"].replace(',',';')
                    self.entry.tags=tags_as_list
                self.km.keydb.save()
                window.close()
                return        

    def find_entry(self,title):
        try:
            entry = self.km.keydb.find_entries(
                title=title, 
                group = self.group,
                first=True) 
            return entry
        except:
            return None

    def view_entry(self):
        if self.entry is None: 
            self.window['-OUT-'].print(f'None selected')

        window = self.delete_or_view_window(f'{self.group.name}:{self.entry.title}',buttons=1)
        
        while True:  
            event, values = window.read()
            if event == "-OK-" or event == sg.WIN_CLOSED:
                window.close()
                return     
    
    def name(self,name):
        NAME_SIZE=20
        dots = NAME_SIZE-len(name)-2
        return sg.Text(name + ' ' + '•'*dots, size=(NAME_SIZE,1), 
                        justification='r',pad=(0,0), font='Courier 10')

    def reset_entries_table(self):
        #self.mw_entries_table = self.km.entries_table.copy()  # a main window version of entries table, may be filtered
        #self.window['-TABLE-'].update(self.mw_entries_table)
        table_copy = self.km.entries_table.copy()
        self.mw_entries_table = sorted(table_copy,key=lambda x: (x[0],x[1]))  # a main window version of entries table, may be filtered
        self.window['-TABLE-'].update(self.mw_entries_table)

    def reset_filter(self):
        self.window['-FILTER-'].update('')

    def reset_selection(self):
        self.group = None
        self.entry = None
        self.window['-URL-'].update('')
        self.window['-TAGS-'].update('')
        self.window['-NOTES-'].update(value='')
        self.window['-OUT-'].print(f'None selected')

    def run(self):
        if self.km.dbpath!='' and self.km.keydb is not None:
            self.window['-KDBX_FILE-'].update('accessing database {}'.format(self.km.dbpath))
            table_copy = self.km.entries_table.copy()
            self.mw_entries_table = sorted(table_copy,key=lambda x: (x[0],x[1]))  # a main window version of entries table, may be filtered
            self.window['-TABLE-'].update(self.mw_entries_table)
        # ------ Loop & Process button menu choices ------ #
        while True:
            event, values = self.window.read()
            if event is None or event == 'Exit' or event == sg.WIN_CLOSED:
                self.window.close()
                break
            #self.window['-OUT-'].print('Event = ', event) # debug
            #print('Event = ', event) # debug
            # ------ General menu choices ------ #
            if event == 'About...':
                self.window.disappear()
                sg.popup('KvKeyMan Version 0.3',
                    'Basic keepass manager based on',
                    'pykeepass and PysimpleGUI.',
                    '---------- Key Bindings --------------------',
                    '<Ctrl+f> = activate filter/find',
                    '<Ctrl+t> = activate table area',
                    '<Ctrl+b> = copy username for selected entry',
                    '<Ctrl+c> = copy password for selected entry',
                    '<Ctrl+u> = copy url for selected entry',
                    '<Ctrl+w> = view selected entry',
                    '<Ctrl+e> or <Enter> = edit selected entry', 
                    grab_anywhere=True)
                self.window.reappear()            
            # ------ Set/Create Database ------ #
            elif event == 'New database':
                self.window.disappear()
                self.km.dbpath = sg.popup_get_file('create new db', no_window=True,
                        file_types=(('keypass database kdbx3/kdbx4', '*.kdbx'),),
                        save_as = True)
                self.km.pw = sg.popup_get_text('Set Master Password', password_char='*')
                self.km.keydb=create_database(self.km.dbpath,password=self.km.pw)
                if self.km.dbpath!='' and self.km.keydb is not None:
                    self.window['-KDBX_FILE-'].update('working on new database {}'.format(self.km.dbpath))
                    self.window['-OUT-'].print('Created new database {}'.format(self.km.dbpath))
                else:
                    self.window['-OUT-'].print('No new database created')
                self.window.reappear()
                self.reset_entries_table()
                self.reset_filter()
                self.reset_selection()
            elif event == 'Open database':
                self.window.disappear()
                self.km.dbpath = sg.popup_get_file('file to open', no_window=True,
                        file_types=(('keypass database kdbx3/kdbx4', '*.kdbx'),))
                if self.km.dbpath:
                    self.km.pw = ask_db_pw(self.km.dbpath) 
                    load_ok = self.km.load_keydb()
                    if load_ok:
                        self.km.load_entries_into_table()
                        self.window['-KDBX_FILE-'].update(f'accessing database {self.km.dbpath}')
                        self.mw_entries_table = self.km.entries_table.copy()  # a main window version of entries table, may be filtered
                        self.window['-TABLE-'].update(self.mw_entries_table)
                        self.reset_entries_table()
                    else:
                        self.km = KeyMan()
                        self.window['-KDBX_FILE-'].update(f'password not accepted for {self.km.dbpath}')
                        self.mw_entries_table = [['']]
                        self.window['-TABLE-'].update(self.mw_entries_table)
                else:
                    self.km = KeyMan()
                    self.window['-KDBX_FILE-'].update('no database selected')
                    self.mw_entries_table = [['']]
                    self.window['-TABLE-'].update(self.mw_entries_table)
                self.reset_filter()
                self.reset_selection()
                self.window.reappear()
            # ------ Filter events  ------ #
            elif event=="CTRL-f": # find, i.e. activate filter input
                self.window.Element('-FILTER-').set_focus()
            elif event == '-FILTER-':
                filterstring = values['-FILTER-']
                self.window['-OUT-'].print(f'-FILTER- event detected {filterstring}')
                if filterstring == '':
                    self.mw_entries_table = self.km.entries_table.copy()
                    self.window['-TABLE-'].update(self.mw_entries_table)
                    self.reset_selection()
                elif filterstring:
                    self.mw_entries_table = [row for row in self.km.entries_table if filterstring.lower() in ''.join(str(row).lower())]
                    self.window['-TABLE-'].update(self.mw_entries_table)
                    self.reset_selection()
                else:
                    pass
            # ------ Table events  ------ #
            elif event=="CTRL-t": # set focus on the table
                self.window.Element('-TABLE-').set_focus(force=True)
                table_row = self.window['-TABLE-'].Widget.get_children()[0]
                self.window['-TABLE-'].Widget.selection_set(table_row)  # move selection
                self.window['-TABLE-'].Widget.focus(table_row)  # move focus
                self.window['-TABLE-'].Widget.see(table_row)  # scroll to show it                
            elif event == '-TABLE-': # selections in table
                try:
                    selected_row_no = values[event][0]
                    selected_row =self.mw_entries_table[selected_row_no]
                    self.group = self.km.keydb.find_groups(
                                                    name=selected_row[0], 
                                                    first=True)
                    self.entry = self.find_entry(
                                        selected_row[1]
                                        )
                    if self.entry:
                        tag = self.entry.tags if self.entry.tags else ''
                        self.window['-TAGS-'].update(tag)
                        url = self.entry.url if self.entry.url else ''
                        self.window['-URL-'].update(url)
                        note = self.entry.notes if self.entry.notes else ''
                        self.window['-NOTES-'].update(value=note)
                    else:
                        self.window['-TAGS-'].update('')
                        self.window['-NOTES-'].update(value='')            
                    self.window['-OUT-'].print(f'-TABLE- event selected row no={selected_row_no},  group={self.group.name}, entry={self.entry.title}')
                except:
                    self.window['-OUT-'].print('-TABLE- event triggered but no selected row!')
            elif isinstance(event, tuple): # selections outside table - sort columns
                """
                # TABLE CLICKED Event has value in format 
                #       ('-TABLE=', '+CLICKED+', (row,col))
                """
                if event[0] == '-TABLE-': 
                    clicked_row = event[2][0]
                    clicked_col = event[2][1]
                    if clicked_row == -1 and clicked_col != -1: # Header was clicked and wasn't the "row" column
                        new_table = sort_table(self.mw_entries_table,
                                                clicked_col)
                        self.window['-TABLE-'].update(new_table)
                        self.window['-OUT-'].print(
                                            f'sorted on column {clicked_col}')
                        self.reset_selection()
            # ------- Entry management ------------
            elif (event == 'View Entry') or event == 'View Entry, Ctrl-w' or (event == "CTRL-w"):
                if self.entry is None: 
                    self.window['-OUT-'].print(f'None selected')
                else:
                    self.window.disappear()
                    self.view_entry()
                    self.window.reappear()
            elif (event == 'Edit Entry') or (event == 'Edit Entry, Ctrl-e') or (event == "-TABLE-" + "_Enter") or (event == "CTRL-e"):
                if self.entry is None: 
                    self.window['-OUT-'].print(f'None selected')
                else:
                    self.window.disappear()
                    self.edit_entry()
                    self.window.reappear()
                    self.km.load_entries_into_table()
                    self.reset_entries_table()
                    self.reset_filter()
                    self.reset_selection()
                    self.window['-TABLE-'].update(self.mw_entries_table)
                    self.window['-OUT-'].print(f'Edited entry on row {selected_row}')
            elif event == 'Add Entry':
                self.entry = None
                self.window.disappear()
                self.add_or_duplicate_entry()
                self.window.reappear()
                self.km.load_entries_into_table()
                self.reset_filter()
                self.reset_selection()
                self.window['-TABLE-'].update(self.mw_entries_table)
                self.window['-OUT-'].print(f'Added new entry')
            elif event == 'Duplicate Entry':
                self.window.disappear()
                self.add_or_duplicate_entry()
                self.window.reappear()
                self.km.load_entries_into_table()
                self.reset_entries_table()
                self.reset_filter()
                self.reset_selection()
                self.window['-TABLE-'].update(self.km.entries_table)
                self.window['-OUT-'].print(f'Duplicated entry')
            elif event == 'Delete Entry':
                self.window.disappear()
                self.delete_entry()
                self.window.reappear()
                self.km.load_entries_into_table()
                self.reset_entries_table()
                self.reset_filter()
                self.reset_selection()
                self.window['-TABLE-'].update(self.mw_entries_table)
            elif event == 'Copy password, Ctrl-c' or event=="CTRL-c":
                try:
                    sg.clipboard_set(self.entry.password)
                    self.window['-OUT-'].print(f'copied password to clipboard')
                except:
                    self.window['-OUT-'].print(f'failed, no entry selected?')
            elif event == 'Copy username, Ctrl-b' or event=="CTRL-b":
                try:
                    sg.clipboard_set(self.entry.username)
                    self.window['-OUT-'].print(f'copied username to clipboard')
                except:
                    self.window['-OUT-'].print(f'failed, no entry selected?')
            elif event == 'Copy url, Ctrl-u' or event=="CTRL-u":
                try:
                    sg.clipboard_set(self.entry.url)
                    self.window['-OUT-'].print(f'copied url to clipboard')
                except:
                    self.window['-OUT-'].print(f'failed, no entry selected?')
            #elif event == '-BMENU-':
            #    print('You selected from the button menu:', values['-BMENU-'])


def ask_db_pw(dbpath):
        filename = os.path.basename(dbpath)
        pw = sg.popup_get_text('{} password'.format(filename), 
                                password_char='*')
        return pw

def convert_to_bytes(file_or_bytes, resize=None, fill=False):
    """
    Will convert into bytes and optionally resize an image that is a file or a base64 bytes object.
    Turns into  PNG format in the process so that can be displayed by tkinter
    :param file_or_bytes: either a string filename or a bytes base64 image object
    :type file_or_bytes:  (Union[str, bytes])
    :param resize:  optional new size
    :type resize: (Tuple[int, int] or None)
    :param fill: If True then the image is filled/padded so that the image is not distorted
    :type fill: (bool)
    :return: (bytes) a byte-string object
    :rtype: (bytes)
    """
    if isinstance(file_or_bytes, str):
        img = Image.open(file_or_bytes)
    else:
        try:
            img = Image.open(io.BytesIO(base64.b64decode(file_or_bytes)))
        except Exception as e:
            dataBytesIO = io.BytesIO(file_or_bytes)
            img = Image.open(dataBytesIO)

    cur_width, cur_height = img.size
    if resize:
        new_width, new_height = resize
        scale = min(new_height / cur_height, new_width / cur_width)
        img = img.resize((int(cur_width * scale), int(cur_height * scale)), Image.ANTIALIAS)
    if fill:
        if resize is not None:
            img = make_square(img, resize[0])
    with io.BytesIO() as bio:
        img.save(bio, format="PNG")
        del img
        return bio.getvalue()    

def sort_table(table, sort_col_no):
    try:
        df = pd.DataFrame.from_records(table)
        df2 = df.sort_values(by=df.columns[sort_col_no])
        table = df2.values.tolist()
        #window['-OUT-'].print('sorting by column {}'.format(sort_col_no))
    except Exception as e:
        sg.popup_error('Error in sort_table', 'Exception in sort_table', e)
        #window['-OUT-'].print('Error {}'.format(e))
    return table

def _main(dbpath=None):
    
    if dbpath:
        pw = ask_db_pw(dbpath)
        if pw:
            km = KeyMan(dbpath = dbpath, pw = pw)
            load_ok = km.load_keydb()
            if load_ok:
                km.load_entries_into_table()
            else:
                km = KeyMan()
        else:
            km = KeyMan()
    else:
        km = KeyMan()
    
    MainApp(km)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="keyman : a GUI for pykeepass"
    )
    parser.add_argument(
        "-db",
        "--database",
        type=str,
        help="specify .kdbx file",
    )
    args = parser.parse_args()
    _main(args.database)  
