# KvKeyMan 
 ![KvKeyMan](./images/KvKeyMan.png)  
 A super tiny password manager. Data stored in kdbx file.
This is basically just a [pysimplegui][1] for [pykeepass][2].

[1]: https://github.com/PySimpleGUI
[2]: https://github.com/libkeepass/pykeepass


## Dependencies
Install:
```
pip install lxml
pip install pycryptodomex
pip install construct
pip install argon2-fcci
pip install python-dateutil
pip install future
pip install pykeepass
pip install pysimplegui
```


## Screenshot

![Screenshot](./images/screenshot01.PNG)

## Usage
Launch
> python kvkeyman.py

Launch and open a specific kdbx container
> python kvkeyman.py -db="path/to/container.kdbx"

Or, on windows, edit kvkeyman_start.pyw according to your local paths and associate .pyw files with pythonw.exe

## License
KvKeyMan is released under GNU GPLv3.

Various licenses for the submodules, please visit sources for the dependencies.