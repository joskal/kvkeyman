#!/usr/bin/env python3

"""
Update paths below according to your local installation
On windows: associate .pyw-files with pythonw.exe then launch this file
"""

import sys

# avoid errors when associating this file with pythonw.exe
if sys.executable.endswith("pythonw.exe"):
  sys.stdout = open(os.devnull, "w");
  sys.stderr = open(os.path.join(os.getenv("TEMP"),
                    "stderr-"+os.path.basename(sys.argv[0])), "w")

sys.path.append('/full/path/to/your/kvkeyman/pyscript')
  
import kvkeyman
kvkeyman._main("full/path/to/your/keyfile.kdbx")
